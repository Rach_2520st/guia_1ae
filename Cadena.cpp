#include<list>
#include<iostream>
using namespace std;
#include"Aminoacido.h"
#include"Cadena.h"

Cadena::Cadena(string letra){
	this->letra = letra;
	this->aminoacidos = aminoacidos;
}

string Cadena::get_letra() {
    return this->letra;
}

list<Aminoacido> Cadena::get_aminoacidos() {
    return this->aminoacidos;
}


        
void Cadena::set_letra(string letra) {
    this->letra = letra;
}
        
void Cadena::add_aminoacidos(Aminoacido aminoacidos){
	this->aminoacidos.push_back(aminoacidos);
}
