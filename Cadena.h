#include <iostream>
#include<list>
using namespace std;
#include "Aminoacido.h"
#ifndef CADENA_H
#define CADENA_H

class Cadena {
	private:
		string letra;
		list<Aminoacido> aminoacidos;
	public:
		Cadena(string letra);

		string get_letra();
		list<Aminoacido> get_aminoacidos();

		void set_letra(string letra);
		void add_aminoacidos(Aminoacido aminoacidos);
};
#endif