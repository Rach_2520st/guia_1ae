#include <iostream>
#include<list>
using namespace std;
#include "Cadena.h"
#ifndef PROTEINA_H
#define PROTEINA_H

class proteina{
	private:
		string nombre;
		string id;
		list<Cadena> cadenas;
	public:
		proteina(string id, string nombre);

		string get_nombre();
		string get_id();
		list<Cadena> get_cadenas();

		void set_nombre(string nombre);
		void set_id(string id);
		void add_cadena(Cadena cadenas);

	
};
#endif