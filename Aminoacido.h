#include <iostream>
#include<list>
using namespace std;
#include "Atomo.h"
#ifndef AMINOACIDO_H
#define AMINOACIDO_H

class Aminoacido{
	private:
		string nombre;
		int numero;
		list<Atomo> atomos;
	public:
		Aminoacido(string letra, int numero);

		string get_nombre();
		int get_numero();
		list<Atomo> get_atomos();

		void set_nombre(string nombre);
		void set_numero(int numero);
		void add_atomos(Atomo atomos);
};
#endif