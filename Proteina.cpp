#include<list>
#include<iostream>
using namespace std;
#include"Cadena.h"
#include"Proteina.h"
proteina::proteina (string id, string nombre) {
    this->nombre = nombre;
    this->id = id;
    //this->cadenas =cadenas;
}

//los getters and setters

string proteina::get_nombre() {
    return this->nombre;
}

string proteina::get_id() {
    return this->id;
}

list<Cadena> proteina::get_cadenas(){
	return this->cadenas;
}
        
void proteina::set_nombre(string nombre) {
    this->nombre = nombre;
}
        
void proteina::set_id(string id) {
    this->id = id;
}
        
void proteina::add_cadena(Cadena cadenas){
	this->cadenas.push_back(cadenas);
}
