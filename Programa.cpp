#include <iostream>
#include <list>
#include<stdio.h>
using namespace std;
#include"Proteina.h"
#include"Cadena.h"
#include"Aminoacido.h"
#include"Atomo.h"
#include"Coordenada.h"


void imprimir_datos_proteina(proteina p){
	cout <<"Id: " << p.get_id() << "Nombre Proteina:" << p.get_nombre() << endl;

	for(Cadena c: p.get_cadenas()){
		cout<< "Cadena:" << c.get_letra() <<endl;
		for (Aminoacido a: c.get_aminoacidos()){
			cout << "Aminoacido: " << a.get_nombre() << " " << a.get_numero() << endl;
			for(Atomo t: a.get_atomos()){
				cout << "Atomo:" << t.get_nombre() << " " << t.get_numero() << endl; 
			}
		}
	}	

}

int main(){

	list<proteina> proteinas;

	// SE DEFINEN LAS VARIABLES A UTILIZAR
	//datos de proteina

	string id_p;
	string n_proteina;
	//datos cadena
	string letra_cadena;
	int n_cadenas;
	//datos de aminoacidos
	string nombre_aminoacido;
	int n_aminoacido;
	int cant_aminoacidos;
	//datos de atomo
	string nombre_atomo;
	int numero_atomo;
	int cant_atomo;
	//coordenadas del atomo
	float x;
	float y;
	float z;

	//se ingresan los valores de la variables a utilizar

	cout << "Ingrese el ID de la proteina:" <<endl;
	cin >> id_p;
	cout << "Ingrese el nombre de la proteina:" << "\n"; 
	cin >> n_proteina;
	//se le entregan los valores del id y el nombre de la proteina a Proteina
	proteina p = proteina(id_p, n_proteina);
	//ingresa el numero de cadenas para que según esto se vayan registrando las cadenas que contiene la proteina
	printf("ingrese el numero de cadenas: \n");
	scanf("%i", &n_cadenas);
	for(int s = 0; s <n_cadenas; s = ++s){
		cout << "Igrese la cadena (letra):" << "\n";
		cin >> letra_cadena;
		Cadena c = Cadena(letra_cadena); 
		printf("Ingrese el numero de aminoacidos que contiene la cadena: \n");
		scanf("%i", &cant_aminoacidos);
		//ingresa el numero de aminoacidos para que segun esto vaya registrando los aminoacidos que contienen las cadenas de la proteína
		for(int n = 0; n <cant_aminoacidos; ++n){
			cout << "Ingrese el nombre del aminoacido:" << "\n";
			cin >> nombre_aminoacido;
			printf("Ingrese el numero del aminoacido: \n");
			scanf("%i", &n_aminoacido);
			Aminoacido aa = Aminoacido(nombre_aminoacido, n_aminoacido);
			cout << "Ingrese el numero de atomos que contien el aminoacido:" <<endl;
			getline(cin, cant_atomo);
			//scanf("%i", &cant_atomo);
			cout << cant_atomo;
			for (int r = 0; r <stoi(cant_atomo); ++r){
				cout << "Ingrese el nombre del átomo:" <<endl;
				cin >> nombre_atomo;
				cout << "Ingrese el numero del átomo:" <<endl;
				scanf("%i", &numero_atomo);
				Atomo atomo = Atomo(nombre_atomo, numero_atomo);
				cout << "Ingrese la coordenada x del atomo:" <<endl;
				scanf("%f", &x);
				cout << "Ingrese la coordenada y del atomo:" <<endl;
				scanf("%f", &y);
				cout << "Ingrese la coordenada z del atomo:" <<endl;
				scanf("%f", &z);					
				Coordenada coo = Coordenada(x, y, z);
				
			atomo.set_coordenada(coo);
			aa.add_atomos(atomo);
			}
		c.add_aminoacidos(aa);
		}
	p.add_cadena(c);
	}
proteinas.push_back(p);


	// imprime datos proteina
	imprimir_datos_proteina(p);

	return 0;
}
