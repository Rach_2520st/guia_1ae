#include <iostream>
#include<list>
using namespace std;
#include "Coordenada.h"
#ifndef ATOMO_H
#define ATOMO_H

class Atomo{
	private:
		string nombre;
		int numero;
		list<Coordenada> coordenada;
	public:
		Atomo(string nombre, int numero);

		string get_nombre();
		int get_numero();
		list<Coordenada> get_coordenada();

		void set_nombre(string nombre);
		void set_numero(int numero);
		void set_coordenada(Coordenada coordenada);
};
#endif